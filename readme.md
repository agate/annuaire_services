# Annuaire des services

## Origine
Cet annuaire des services est élaboré dans le cadre du Schéma Départemental d'Amélioration de l'Accessibilité des Services à la Population (**SDAASP**) de la Savoie.
Ce schéma a été validé en juin 2018 par le Conseil départemental, et doit être approuvé par le Préfet d'ici la fin de l'année.
Cet annuaire correspond à la fiche action 3.3.2 (_Développer l'information à destination du grand public_)

## Specifications techniques
Cet annuaire est pensé et construit pour être le réceptacle de données se trouvant dans différentes bases données, afin de limiter les doublons et complexifier la mise à jour. Ainsi l'annuaire prendra comme source de données l'annuaire de l'administration [Service-public.fr] (https://www.service-public.fr/)

### Hébergement et maintien
Cet annuaire a vocation à être l'outils des acteurs du territoire, à commencer par les deux co-pilotes du SDAASP, la préfecture et le Conseil départemental.
Techniquement l'outils est forgé et porté dans sa phase de développement par @Agate