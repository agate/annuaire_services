//-------------------------------------------  LEGENDE

/* Ancien Control des couches  
  var controlLayers = L.control.layers(null, null, { //Controle des couches à supprimer à terme car géré dans le bloc légende de l'app
     position: "topright",
     scollapsed: false
 }).addTo(map);
 */

function init_legend() {

      // ETAPE 1 :  INTERACTIVITE MENU DEROULANT
  // On récupère la balise accordion <=> les titres principaux + picto_legende
  var acc = document.getElementsByClassName("accordion");
  var i;

  // Boucle pour le menu déroulant
  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.maxHeight){
        panel.style.maxHeight = null;
      } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
      } 
    });
  }

// Non utilisés, c'est l'intéractivité d'une barre de recherche
/* $(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#legend label").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
}); */

      // ETAPE 2 : INTERACTIVITE CHECKBOX
    // FAMILLE 
// ATTENTION : geoJsonPMI.active définit dans le fichier carte.js 
// CAF
$('#caf_famille').click(function (e) {
  if (geoJsonCafFamille.active === false) {
    map.addLayer(markerClustersFamilleCaf);
    geoJsonCafFamille.active = true;
  } else if (geoJsonCafFamille.active === true) {
    map.removeLayer(markerClustersFamilleCaf);
    geoJsonCafFamille.active = false;
  }
});

// Msa
$('#msa_famille').click(function (e) {
  if (geoJsonMsa.active === false) {
    map.addLayer(markerClustersFamilleMsa);
    geoJsonMsa.active = true;
  } else if (geoJsonMsa.active === true) {
    map.removeLayer(markerClustersFamilleMsa);
    geoJsonMsa.active = false;
  }
});
// PMI
$('#pmi_famille').click(function (e) {
  if (geoJsonPMI.active === false) {
    // ajout des clusters à la carte  
    map.addLayer(markerClustersFamillePmi);
    geoJsonPMI.active = true;
    console.log(geoJsonPMI.active);
  } else if (geoJsonPMI.active === true) {
    // enlève les clusters à la carte 
    map.removeLayer(markerClustersFamillePmi);
    geoJsonPMI.active = false;
    console.log(geoJsonPMI.active);
  }
});

    // SANTE
// CAF
$('#caf_sante').click(function (e) {
  if (geoJsonCafSante.active === false) {
    map.addLayer(markerClustersSanteCaf);
    geoJsonCafSante.active = true;
  } else if (geoJsonCafSante.active === true) {
    map.removeLayer(markerClustersSanteCaf);
    geoJsonCafSante.active = false;
  }
});

// CARSAT
$('#carsat_sante').click(function (e) {
  if (geoJsonCarsatSante.active === false) {
    map.addLayer(markerClustersSanteCarsat);
    geoJsonCarsatSante.active = true;
  } else if (geoJsonCafSante.active === true) {
    map.removeLayer(markerClustersSanteCarsat);
    geoJsonCarsatSante.active = false;
  }
});

// CPAM
$('#cpam_sante').click(function (e) {
  if (geoJsonSanteCpam.active === false) {
    map.addLayer(markerClustersSanteCpam);
    geoJsonSanteCpam.active = true;
  } else if (geoJsonSanteCpam.active === true) {
    map.removeLayer(markerClustersSanteCpam);
    geoJsonSanteCpam.active = false;
  }
});

// MSAP
$('#msap_sante').click(function (e) {
  if (geoJsonMsapSante.active === false) {
    map.addLayer(markerClustersSanteMsap);
    geoJsonMsapSante.active = true;
  } else if (geoJsonMsapSante.active === true) {
    map.removeLayer(markerClustersSanteMsap);
    geoJsonMsapSante.active = false;
  }
});


    // EMPLOI
// Pole emploi
$('#pole_emploi').click(function (e) {
  if (geoJsonPoleEmploi.active === false) {
    map.addLayer(markerClustersEcoEmploi);
    geoJsonPoleEmploi.active = true;
  } else if (geoJsonPoleEmploi.active === true) {
    map.removeLayer(markerClustersEcoEmploi);
    geoJsonPoleEmploi.active = false;
  }
});

// Mission locale
$('#mission_emploi').click(function (e) {
  if (geoJsonMissionLocale.active === false) {
    map.addLayer(markerClustersEcoMission);
    geoJsonMissionLocale.active = true;
  } else if (geoJsonMissionLocale.active === true) {
    map.removeLayer(markerClustersEcoMission);
    geoJsonMissionLocale.active = false;
  }
});
// MSAP
$('#msap_emploi').click(function (e) {
  if (geoJsonMsapEmploi.active === false) {
    map.addLayer(markerClustersEcoMsap);
    geoJsonMsapEmploi.active = true;
  } else if (geoJsonMsapEmploi.active === true) {
    map.removeLayer(markerClustersEcoMsap);
    geoJsonMsapEmploi.active = false;
  }
});
    // ADMIN
// MARIE
$('#marie_admin').click(function (e) {
  if (geoJsonMarieAdmin.active === false) {
    map.addLayer(markerClustersAdminMarie);
    geoJsonMarieAdmin.active = true;
  } else if (geoJsonMarieAdmin.active === true) {
    map.removeLayer(markerClustersAdminMarie);
    geoJsonMarieAdmin.active = false;
  }
});
// MSAP
$('#msap_admin').click(function (e) {
  if (geoJsonMsapAdmin.active === false) {
    map.addLayer(markerClustersAdminMsap);
    geoJsonMsapAdmin.active = true;
  } else if (geoJsonMsapAdmin.active === true) {
    map.removeLayer(markerClustersAdminMsap);
    geoJsonMsapAdmin.active = false;
  }
});
// IMPOTS
$('#impot_admin').click(function (e) {
  if (geoJsonImpotAdmin.active === false) {
    map.addLayer(markerClustersAdminImpot);
    geoJsonImpotAdmin.active = true;
  } else if (geoJsonImpotAdmin.active === true) {
    map.removeLayer(markerClustersAdminImpot);
    geoJsonImpotAdmin.active = false;
  }
});






};