
//-------------------------------------------  INTERACTIVITE MAP

function init_map() {




               //ETAPE 1: Configration générale de la map
  map = L.map('map', {
    minZoom: 9,
    maxZoom: 16,
    keyboard: false,
    attributionControl: false,
/*     maxBounds: [
      [45, 5.4],
      [46, 7.3]
    ] */
  }).setView([45.5, 6.3], 9);

  // Supression du double click - Zoom
  map.doubleClickZoom.disable();

  // Contribution
  var sources = L.control.attribution({position: 'bottomright'}).addTo(map);
  sources.addAttribution("Réalisé par Agence Alpin des Territoires avec Leaflet"); 

  // Position Zoom Control
  map.zoomControl.setPosition('bottomright'); 






               // ETAPE 2 : Fond de carte 
  // Fond de carte mapstamen
  var layer = new L.StamenTileLayer("terrain");
  map.addLayer(layer);

 // Fond de carte du département de Savoie
  var savoieJs = L.geoJson(savoie, {
    style: {
      weight:3,
      opacity:0.8,
      color: "#333333",
      fillOpacity: 0},
   
  }).addTo(map);


  // Fond de carte des communes de Savoie
/*   communesJs = L.geoJson(com, {

    style: {
      weight:1,
      opacity:0.8,
      color: "#333333",
      fillOpacity: 0},
    
  }).addTo(map);
 */

  




               // ETAPE 3 : CLUSTERS

          //Création des markers customisés par thématiques
  // Définition de la classe markers
  var marker = L.Icon.extend({
    options: {
      iconSize: [40, 45],
      iconAnchor: [20, 42]
    }
  });

  // Création des icones par thématique avec la classe marker
  var familleMarker = new marker({ iconUrl: 'css/img/marker_famille.png' });
  var santeMarker = new marker({ iconUrl: 'css/img/marker_sante.png' });
  var emploiMarker = new marker({ iconUrl: 'css/img/marker_emploi.png' });
  var adminiMarker = new marker({ iconUrl: 'css/img/marker_admin.png' });
 



  //Création des groupes de clusters customisés par thématiques
  
  markerClustersFamilleCaf = L.markerClusterGroup({
    maxClusterRadius: 120,
    iconCreateFunction: function (cluster) {
      var markers = cluster.getAllChildMarkers();
      var html = '<div class="cluster_famille">' + markers.length + '</div>';
      return L.divIcon({ html: html, className: 'cluster_famille', iconSize: L.point(32, 32) });
    }
  });

  markerClustersFamilleMsa = L.markerClusterGroup({
    maxClusterRadius: 120,
    iconCreateFunction: function (cluster) {
      var markers = cluster.getAllChildMarkers();
      var html = '<div class="cluster_famille">' + markers.length + '</div>';
      return L.divIcon({ html: html, className: 'cluster_famille', iconSize: L.point(32, 32) });
    }
  });
  
  markerClustersFamillePmi = L.markerClusterGroup({
    maxClusterRadius: 120,                                   // Important définit le rayon de clusteristaion
    iconCreateFunction: function (cluster) {
      // recupére le nombre de point dans le cluster
      var markers = cluster.getAllChildMarkers();     
      // insert une div html contenant le nombre de point pour chaque cluster
      var html = '<div class="cluster_famille">' + markers.length + '</div>'; 
      // retourne une divIcon avec la taille de l'icon et son contenu (nombre de point dans un cluster)  
      return L.divIcon({ html: html, className: 'cluster_famille', iconSize: L.point(32, 32) });
    }
  });

  markerClustersFamilleUdaf = L.markerClusterGroup({
    maxClusterRadius: 120,
    iconCreateFunction: function (cluster) {
      var markers = cluster.getAllChildMarkers();
      var html = '<div class="cluster_famille">' + markers.length + '</div>';
      return L.divIcon({ html: html, className: 'cluster_famille', iconSize: L.point(32, 32) });
    }
  });

  markerClustersSanteCaf = L.markerClusterGroup({
    maxClusterRadius: 120,
    iconCreateFunction: function (cluster) {
      var markers = cluster.getAllChildMarkers();
      var html = '<div class="cluster_sante">' + markers.length + '</div>';
      return L.divIcon({ html: html, className: 'cluster_sante', iconSize: L.point(32, 32) });
    }
  });

  markerClustersSanteCarsat = L.markerClusterGroup({
    maxClusterRadius: 120,
    iconCreateFunction: function (cluster) {
      var markers = cluster.getAllChildMarkers();
      var html = '<div class="cluster_sante">' + markers.length + '</div>';
      return L.divIcon({ html: html, className: 'cluster_sante', iconSize: L.point(32, 32) });
    }
  });

  markerClustersSanteCpam = L.markerClusterGroup({
    maxClusterRadius: 120,
    iconCreateFunction: function (cluster) {
      var markers = cluster.getAllChildMarkers();
      var html = '<div class="cluster_sante">' + markers.length + '</div>';
      return L.divIcon({ html: html, className: 'cluster_sante', iconSize: L.point(32, 32) });
    }
  });

  markerClustersSanteCpas = L.markerClusterGroup({
    maxClusterRadius: 120,
    iconCreateFunction: function (cluster) {
      var markers = cluster.getAllChildMarkers();
      var html = '<div class="cluster_sante">' + markers.length + '</div>';
      return L.divIcon({ html: html, className: 'cluster_sante', iconSize: L.point(32, 32) });
    }
  });

  markerClustersSanteMsap = L.markerClusterGroup({
    maxClusterRadius: 120,
    iconCreateFunction: function (cluster) {
      var markers = cluster.getAllChildMarkers();
      var html = '<div class="cluster_sante">' + markers.length + '</div>';
      return L.divIcon({ html: html, className: 'cluster_sante', iconSize: L.point(32, 32) });
    }
  });

  markerClustersEcoEmploi = L.markerClusterGroup({
    maxClusterRadius: 120,
    iconCreateFunction: function (cluster) {
      var markers = cluster.getAllChildMarkers();
      var html = '<div class="cluster_emploi">' + markers.length + '</div>';
      return L.divIcon({ html: html, className: 'cluster_emploi', iconSize: L.point(32, 32) });
    }
  });

  markerClustersEcoMission = L.markerClusterGroup({
    maxClusterRadius: 120,
    iconCreateFunction: function (cluster) {
      var markers = cluster.getAllChildMarkers();
      var html = '<div class="cluster_emploi">' + markers.length + '</div>';
      return L.divIcon({ html: html, className: 'cluster_emploi', iconSize: L.point(32, 32) });
    }
  });

  markerClustersEcoMsap = L.markerClusterGroup({
    maxClusterRadius: 120,
    iconCreateFunction: function (cluster) {
      var markers = cluster.getAllChildMarkers();
      var html = '<div class="cluster_emploi">' + markers.length + '</div>';
      return L.divIcon({ html: html, className: 'cluster_emploi', iconSize: L.point(32, 32) });
    }
  });

  markerClustersAdminMarie = L.markerClusterGroup({
    maxClusterRadius: 120,
    iconCreateFunction: function (cluster) {
      var markers = cluster.getAllChildMarkers();
      var html = '<div class="cluster_admin">' + markers.length + '</div>';
      return L.divIcon({ html: html, className: 'cluster_admin', iconSize: L.point(32, 32) });
    }
  });

  markerClustersAdminMsap = L.markerClusterGroup({
    maxClusterRadius: 120,
    iconCreateFunction: function (cluster) {
      var markers = cluster.getAllChildMarkers();
      var html = '<div class="cluster_admin">' + markers.length + '</div>';
      return L.divIcon({ html: html, className: 'cluster_admin', iconSize: L.point(32, 32) });
    }
  });

  markerClustersAdminImpot = L.markerClusterGroup({
    maxClusterRadius: 120,
    iconCreateFunction: function (cluster) {
      var markers = cluster.getAllChildMarkers();
      var html = '<div class="cluster_admin">' + markers.length + '</div>';
      return L.divIcon({ html: html, className: 'cluster_admin', iconSize: L.point(32, 32) });
    }
  });


  



               // ETAPE 3 : POPUP ET CONTENU (API)
  //Fonction evenement pour afficher les infos dans le panneau info sur le clic de chaque maker
  function evenement(feature, layer) {

    // Permet de vérifier les différentes conditions dans la récupération du contenu des popup
    layer.feature.properties.legend = "1"
    
    /* console.log(layer.feature.properties.legend); */
    layer.on('click', function (e) {
      // On supprime le div par défaut lors du clic
      $('#infobar').remove();

            // ADRESSE INTERNET
      var url = "";
      switch (layer.feature.properties.pivotLocal) {      // récupérer le nom des services dans pivotLocal
        case 'caf':
        url = 'https://www.caf.fr/';
        break;
        case 'msa':
        url = 'http://www.msa.fr/';
        break;
        case 'pmi':
        url = 'http://www.savoie.fr/resultat_profil/profil/9/2758-infos-pratiques.htm';
        break;
        case 'msap':
        url = 'https://www.maisondeservicesaupublic.fr/';
        break;
        case 'cpam':
        url = 'https://www.ameli.fr/';
        break;
        case 'pole_emploi':
        url = 'https://www.pole-emploi.fr/accueil/';
        break;
        case 'mission_locale':
        url = 'https://www.mission-locale.fr/';
        break;
        case 'tresorerie':
        url = 'https://www.impots.gouv.fr/';
        break;
        case 'mairie':
        url = layer.feature.properties.CoordonnéesNum.Url;
        break;
        default:
        url = 'Absence de ressource internet';

      }
      


            // NUMERO TELEPHONE
      var tel = "";
      // Il peuvent être sous forme de chaîne ou d'objet
      if (typeof layer.feature.properties.CoordonnéesNum.Téléphone === "string") {
        tel = layer.feature.properties.CoordonnéesNum.Téléphone;
      }else if (typeof layer.feature.properties.CoordonnéesNum.Téléphone === "object") {
        // $t sous catégorie de télephone 
        tel = layer.feature.properties.CoordonnéesNum.Téléphone.$t;
      }
       

            // ADRESSE
      // Si l'adresse est un objet et non un array (tableau)
      // typeof opérateur indiquant le type d'opérand (objet, nombre, string...)
      // Array.isArray méthode permettant de déterminer si un objet est un array (tableau)
      if (typeof layer.feature.properties.Adresse === 'object' && Array.isArray(layer.feature.properties.Adresse) === false) {
        // l'adresse est dans l'objet adresse avec la cléf-valeur 'Ligne'
        // Ligne peut être une simple chaîne ou un array contenant plusieurs chaînes
        var adresseArray = layer.feature.properties.Adresse.Ligne;
        var adresseString = "";
        codePostalString = layer.feature.properties.Adresse.CodePostal;
        nomCommuneString = layer.feature.properties.Adresse.NomCommune;
        // Si ligne est une simple chaîne, on affiche la string
        if (typeof adresseArray === "string") {
          adresseString = adresseArray;

        // si ligne est un array contenant plusieurs string 
        } else {
          //on boucle dans la liste pour afficher toutes les lignes
          for (i = 0; i < adresseArray.length; i++) {
            adresseString = adresseString + adresseArray[i] + " ";
          }
        }
      
      

      // Si l'adresse est un array (tableau)
      } else if (Array.isArray(layer.feature.properties.Adresse) === true) {
        //Dans ce cas, on prend l'adresse physique, à priori indexé en [0] mais à vérifier...
        var adresseArray = layer.feature.properties.Adresse[0].Ligne;
        /* console.log(layer.feature.properties); */
        var adresseString = ""; 

        // Si adresse sur une seule ligne, on affiche la string
        if (typeof adresseArray === "string") {
          adresseString = adresseArray;

        //Si adresse stockées sur plusieurs ligne, dans une array, on boucle dans la liste pour afficher toutes les lignes
        } else {
          for (i = 0; i < adresseArray.length; i++) {
            adresseString = adresseString + adresseArray[i] + " ";
          }
        }
        adresseString = adresseString + " " ;
        codePostalString = layer.feature.properties.Adresse[0].CodePostal;
        nomCommuneString = layer.feature.properties.Adresse[0].NomCommune;
      // On indique que l'adresse n'est pas renseigné
      } else {
        adresseString = "Adresse non communiquée";
      }




      

          // LES HORAIRES 
      console.log(layer.feature.properties.Ouverture);
      // STEP 1 : PlageJ est un objet contenant plageH
      if (typeof layer.feature.properties.Ouverture.PlageJ === "object" && Array.isArray(layer.feature.properties.Ouverture.PlageJ) === false) {//Si les jours d'ouvertures sont stockés dans un objet
        var ouvertureJ = layer.feature.properties.Ouverture.PlageJ.début + " - " + layer.feature.properties.Ouverture.PlageJ.fin ;
        console.log(layer.feature.properties.Ouverture.PlageJ); //Jour pour PlageJ

        // 1.1 Plage H est un objet     
        if (typeof layer.feature.properties.Ouverture.PlageJ.PlageH === "object" && Array.isArray(layer.feature.properties.Ouverture.PlageJ.PlageH) === false) {//Si les horaires d'ouvertures sont stockés dans un objet
          var ouvertureH = layer.feature.properties.Ouverture.PlageJ.PlageH.début.slice(0, 5) + " " + layer.feature.properties.Ouverture.PlageJ.PlageH.fin.slice(0, 5);
          console.log(ouvertureH); // Horaire de plage H

        // 1.2  Plage H est un array          
        } else if (Array.isArray(layer.feature.properties.Ouverture.PlageJ.PlageH) === true) {
          var plageJ= layer.feature.properties.Ouverture.PlageJ.PlageH
          var resultH = [];
          // 1.2.1 Chaque plageH est rangé dans un objet
          for (var i = 0; i < plageJ.length; i++){
            /* console.log(i); */ // 0 et 1 = différente objet
            var temp = plageJ[i] ;

            // Pour récupérer les différentes plage horaire dans chaque objet
            for (j in  temp){
              resultH += temp[j].slice(0, 5) + " "  
            }
          }
          console.log(resultH);

          // 1.3 Horaire final
          var ouvertureH = resultH.slice(0, 11) + " </br> " + resultH.slice(12);
          console.log(ouvertureH);


        // 1.4 Si les horaire ne sont pas disponible
        } else {
          console.log("erreur sur la récupération des horaires d'ouverture !");
          var ouvertureH = "Horaires non communiqués";

        };
        // 1.5 Combinaison des jour et horaire
        ouvertureJ = ouvertureJ + " : </br>" + ouvertureH
        console.log(ouvertureJ);


        // 1.6 Création du div pour les informations
        $("<div id='infobar'><img src='css/img/picto_fermeture.png' alt='Picto fermeture' id='picto_fermeture'><div class='info-titre'>" + 
        layer.feature.properties.Nom + "<br><br><div class='info-adresse'>Adresse : <br><div style='font-weight:normal'>" + 
        adresseString + "</div><div style='font-weight:normal'>" + 
        codePostalString + " " + 
        nomCommuneString + "</div></div><div class='info-ouverture'>Ouverture : <br><div style='font-weight:normal'>" + 
        ouvertureJ + "</div></div><div class='info-adresse'>Téléphone :<br><div style='font-weight:normal'>" + 
        tel + "</div></div><div class='info-internet'><a href='" + 
        url + "' target='_blank'>Lien internet</div></div>").insertBefore('#map');
        /* layer.feature.properties.legend = "2"; */
        /* console.log(layer.feature.properties.legend); */


      // STEP 2 : PlageJ est un tableau contenant plageH    
      } else if (Array.isArray(layer.feature.properties.Ouverture.PlageJ) === true) {
        var plageJ = layer.feature.properties.Ouverture.PlageJ;
        var result2 = "";
        var result1 = "";  

        // 2.1 Entrer dans PlageJ (jour)
        for ( i in plageJ){
          console.log(plageJ[i]); // jour

          // 2.1.1 PlageH est un objet
          if (Array.isArray(plageJ[i].PlageH) === false){
            // Evite les répétition si la plage horaire est sur un jour
            if (plageJ[i].début === plageJ[i].fin ){
              plageJour = plageJ[i].début+ ' :</br> ' 
            }else{
              plageJour = plageJ[i].début + ' - ' + plageJ[i].fin + ' :</br> ' 
            }
            // Création des horaire contenant les jours et les heures
            result1 += plageJour  + plageJ[i].PlageH.début.slice(0, 5) + ' ' + plageJ[i].PlageH.fin.slice(0, 5) + ' </br> ';
            
            
            
            // 2.1.2 PlageH est un tableau
          }else if (Array.isArray(plageJ[i].PlageH) === true){
            for (j in plageJ[i].PlageH){
              if (plageJ[i].début === plageJ[i].fin){
                plageJour = plageJ[i].début+ ' :</br> ' 
              }else{
                plageJour = plageJ[i].début + ' - ' + plageJ[i].fin + ' :</br> ' 
              }
              result2 += plageJour + plageJ[i].PlageH[j].début.slice(0, 5) + ' '+ plageJ[i].PlageH[j].fin.slice(0, 5) + ' </br> ';
                      
          }

          // 2.2 Si les horaire ne sont pas disponible  
          }else {
            console.log("erreur sur la récupération des horaires d'ouverture !");
            var ouvertureH = "Horaires non communiqués";
  
          };       

        }

        // 2.3 Création de ouverture 
        ouvertureJ = result1 + result2

        // 2.4 Création du div pour les informations
        $("<div id='infobar'><img src='css/img/picto_fermeture.png' alt='Picto fermeture' id='picto_fermeture'><div class='info-titre'>" + 
        layer.feature.properties.Nom + "<br><br><div class='info-adresse'>Adresse : <br><div style='font-weight:normal'>" + 
        adresseString + "</div><div style='font-weight:normal'>" + 
        codePostalString + " " + 
        nomCommuneString + "</div></div><div class='info-ouverture'>Ouverture : <br><div style='font-weight:normal'>" + 
        ouvertureJ + "</div></div><div class='info-adresse'>Téléphone :<br><div style='font-weight:normal'>" + 
        tel + "</div></div><div class='info-internet'><a href='" + 
        url + "' target='_blank'>Lien internet</div></div>").insertBefore('#map');
        
      }
    })
  };

  // STEP 3 : FERMETURE DES POPUP  
  $('body').on('click', "#picto_fermeture", function () {
    $('#infobar').remove();      // On enlève les popup quand l'utilisateur clic que le picto fermeture (cf. css)
  }); 







  
               // ETAPE 4 : APPEL API

  /*   //Exemple de marker sans clusters
  $.getJSON("https://etablissements-publics.api.gouv.fr/v1/organismes/73/pmi", function (data) {
    var geoJsonPMI = L.geoJson(data, {
     pointToLayer: function (feature, latlng) {
      return L.marker(latlng, { icon: familleMarker });
    }
  }).addTo(map);
  controlLayers.addOverlay(geoJsonPMI, 'PMI');
}); */


  //Appels sur l'API services publics pour récupérer les geojson
  


  // API CAF_famille
  $.getJSON("https://etablissements-publics.api.gouv.fr/v1/organismes/73/caf", function (data) {
    geoJsonCafFamille = L.geoJson(data, {
      pointToLayer: function (feature, latlng) {
        return L.marker(latlng, { icon: familleMarker });     // mettre un markers scpécifique avec la bonne lat et long
      },
      onEachFeature: evenement        // pour les popup
    });
    geoJsonCafFamille.active = false;        // pour le légend et les checkbox (cf legend.js)
    markerClustersFamilleCaf.addLayer(geoJsonCafFamille);   // ajout du geoJsonCafFamille aux clustersgroup (complète le markers= chiffre, créer les clusters)
  });

    // API MSA_famille
    $.getJSON("https://etablissements-publics.api.gouv.fr/v1/organismes/73/msa", function (data) {
      geoJsonMsa = L.geoJson(data, {
        pointToLayer: function (feature, latlng) {
          return L.marker(latlng, { icon: familleMarker });     // mettre un markers scpécifique avec la bonne lat et long
        },
        onEachFeature: evenement        // pour les popup
      });
      geoJsonMsa.active = false;        // pour le légend et les checkbox (cf legend.js)
      markerClustersFamilleMsa.addLayer(geoJsonMsa);   // ajout du geoJsonCafFamille aux clustersgroup (complète le markers= chiffre, créer les clusters)
    });

    // API PMI_famille
  $.getJSON("https://etablissements-publics.api.gouv.fr/v1/organismes/73/pmi", function (data) {
    // Création d'un geojson à partir de l'API
    geoJsonPMI = L.geoJson(data, {
      pointToLayer: function (feature, latlng) {
        // en fonction de la latitude et longitude return le marker thématique correspondant
        return L.marker(latlng, { icon: familleMarker });
      },
      // appel la fonction evenement pour les popup et leur contenu
      onEachFeature: evenement
    });
    //Ajout d'une propriété à l'objet afin de vérifier son activité (affiché ou non) pour permmettre le contrôle des couches dans notre légende
    // cf.legende.js
    geoJsonPMI.active = false; 
    // on ajout les makers sur la carte avec leur contenu (markerClustersFamillePmi = nombre de pint compris dans le cluster)
    markerClustersFamillePmi.addLayer(geoJsonPMI);
    // map.addLayer(markerClustersFamillePmi);
    //controlLayers.addOverlay(markerClustersFamille, 'PMI');//Contrôle des couches par défaut de leaflet. Décommanter si besoin en développement
  });





   // API CAF_sante
   $.getJSON("https://etablissements-publics.api.gouv.fr/v1/organismes/73/caf", function (data) {
    geoJsonCafSante = L.geoJson(data, {
      pointToLayer: function (feature, latlng) {
        return L.marker(latlng, { icon: santeMarker });     // mettre un markers scpécifique avec la bonne lat et long
      },
      onEachFeature: evenement        // pour les popup
    });
    geoJsonCafSante.active = false;        // pour le légend et les checkbox (cf legend.js)
    markerClustersSanteCaf.addLayer(geoJsonCafSante);   // ajout du geoJsonCafSante aux clustersgroup (complète le markers= chiffre, créer les clusters)
  });

  // API CARSAT_sante
  $.getJSON("https://etablissements-publics.api.gouv.fr/v1/organismes/73/carsat", function (data) {
    geoJsonCarsatSante = L.geoJson(data, {
      pointToLayer: function (feature, latlng) {
        return L.marker(latlng, { icon: santeMarker });     // mettre un markers scpécifique avec la bonne lat et long
      },
      onEachFeature: evenement        // pour les popup
    });
    geoJsonCarsatSante.active = false;        // pour le légend et les checkbox (cf legend.js)
    markerClustersSanteCarsat.addLayer(geoJsonCarsatSante);   // ajout du geoJsonCarsatSante aux clustersgroup (complète le markers= chiffre, créer les clusters)
  });

  // API CPAM_sante
  $.getJSON("https://etablissements-publics.api.gouv.fr/v1/organismes/73/cpam", function (data) {
    geoJsonSanteCpam = L.geoJson(data, {
      pointToLayer: function (feature, latlng) {
        return L.marker(latlng, { icon: santeMarker });
      },
      onEachFeature: evenement
    });
    geoJsonSanteCpam.active = false; //Ajout d'une propriété à l'objet afin de vérifier son activité (affiché ou non) pour permmettre le contrôle des couches dans notre légende
    markerClustersSanteCpam.addLayer(geoJsonSanteCpam);
    //map.addLayer(markerClustersSante);//On ajoute pas tout de suite la couche
    //controlLayers.addOverlay(markerClustersSante, 'CPAM');//Contrôle des couches par défaut de leaflet. Décommanter si besoin en développement
  });

 // API MSAP_sante
 $.getJSON("https://etablissements-publics.api.gouv.fr/v1/organismes/73/msap", function (data) {
  geoJsonMsapSante = L.geoJson(data, {
    pointToLayer: function (feature, latlng) {
      return L.marker(latlng, { icon: santeMarker });
    },
    onEachFeature: evenement
  });
  geoJsonMsapSante.active = false; //Ajout d'une propriété à l'objet afin de vérifier son activité (affiché ou non) pour permmettre le contrôle des couches dans notre légende
  markerClustersSanteMsap.addLayer(geoJsonMsapSante);
});



  // API Pôle emploi
  $.getJSON("https://etablissements-publics.api.gouv.fr/v1/organismes/73/pole_emploi", function (data) {
    geoJsonPoleEmploi = L.geoJson(data, {
      pointToLayer: function (feature, latlng) {
        return L.marker(latlng, { icon: emploiMarker });
      },
      onEachFeature: evenement
    });
    geoJsonPoleEmploi.active = false; 
    markerClustersEcoEmploi.addLayer(geoJsonPoleEmploi);
  });


  // API Mission locale_emploi
  $.getJSON("https://etablissements-publics.api.gouv.fr/v1/organismes/73/mission_locale", function (data) {
    geoJsonMissionLocale = L.geoJson(data, {
      pointToLayer: function (feature, latlng) {
        return L.marker(latlng, { icon: emploiMarker });
      },
      onEachFeature: evenement
    });
    geoJsonMissionLocale.active = false; //Ajout d'une propriété à l'objet afin de vérifier son activité (affiché ou non) pour permmettre le contrôle des couches dans notre légende
    markerClustersEcoMission.addLayer(geoJsonMissionLocale);
    //map.addLayer(markerClustersEmploi);//On ajoute pas tout de suite la couche
    //controlLayers.addOverlay(markerClustersEmploi, 'Pôle emploi');//Contrôle des couches par défaut de leaflet. Décommanter si besoin en développement
  });

  // API MSAP_emploi
  $.getJSON("https://etablissements-publics.api.gouv.fr/v1/organismes/73/msap", function (data) {
    geoJsonMsapEmploi = L.geoJson(data, {
      pointToLayer: function (feature, latlng) {
        return L.marker(latlng, { icon: emploiMarker });
      },
      onEachFeature: evenement
    });
    geoJsonMsapEmploi.active = false; //Ajout d'une propriété à l'objet afin de vérifier son activité (affiché ou non) pour permmettre le contrôle des couches dans notre légende
    markerClustersEcoMsap.addLayer(geoJsonMsapEmploi);
  });

  // API Marie_admin
  $.getJSON("https://etablissements-publics.api.gouv.fr/v1/organismes/73/mairie", function (data) {
    geoJsonMarieAdmin = L.geoJson(data, {
      pointToLayer: function (feature, latlng) {
        return L.marker(latlng, { icon: adminiMarker });
      },
      onEachFeature: evenement
    });
    geoJsonMarieAdmin.active = false; //Ajout d'une propriété à l'objet afin de vérifier son activité (affiché ou non) pour permmettre le contrôle des couches dans notre légende
    markerClustersAdminMarie.addLayer(geoJsonMarieAdmin);
  });

  // API MSAP_admin
  $.getJSON("https://etablissements-publics.api.gouv.fr/v1/organismes/73/msap", function (data) {
    geoJsonMsapAdmin = L.geoJson(data, {
      pointToLayer: function (feature, latlng) {
        return L.marker(latlng, { icon: adminiMarker });
      },
      onEachFeature: evenement
    });
    geoJsonMsapAdmin.active = false; //Ajout d'une propriété à l'objet afin de vérifier son activité (affiché ou non) pour permmettre le contrôle des couches dans notre légende
    markerClustersAdminMsap.addLayer(geoJsonMsapAdmin);
  });

  // API Marie_impots
  $.getJSON("https://etablissements-publics.api.gouv.fr/v1/organismes/73/tresorerie", function (data) {
    geoJsonImpotAdmin = L.geoJson(data, {
      pointToLayer: function (feature, latlng) {
        return L.marker(latlng, { icon: adminiMarker });
      },
      onEachFeature: evenement
    });
    geoJsonImpotAdmin.active = false; //Ajout d'une propriété à l'objet afin de vérifier son activité (affiché ou non) pour permmettre le contrôle des couches dans notre légende
    markerClustersAdminImpot.addLayer(geoJsonImpotAdmin);
  });



}; 
// Fin de la fonction init_map()